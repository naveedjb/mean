
angular.module('example').controller('ExampleController', ['$scope','Authentication',
    function($scope,Authentication) {
        console.log("Example Controller");
        $scope.name =  Authentication.user ? Authentication.user.fullName : 'MEAN Application';
    }
]);