/**
 * Created by Zeeshan on 1/23/2015.
 */

var config=require('./config');
var express=require('express');
var compress=require('compression');
var morgan=require('morgan');
var bodyParser=require('body-parser');
var session=require('express-session');
var methodOveride=require('method-override');
var mongoose=require('mongoose');
var passport=require('passport');
var flash=require('connect-flash');

module.exports=function() {
    var app=express();


    if(process.env.NODE_ENV=='development')
    {
        app.use(morgan('dev'));

    }
    else if(process.env.NODE_ENV=='production')
    {
        app.use(compress())
    }

    app.use(bodyParser.urlencoded({extended:true}));

    app.use(bodyParser.json());

    app.use(methodOveride())

    app.use(session({
        saveInitialized: true,
        resave:true,
        secret:config.sessionSecret

    }))


    app.set('views', './app/views');
    app.set('view engine', 'ejs')

    app.use(flash());
    app.use(passport.initialize());
    app.use(passport.session());


    require('../app/routes/index.server.routes')(app);
    require('../app/routes/users.server.routes')(app);
    app.use(express.static('./public'));
    return app;

}
