/**
 * Created by Zeeshan on 1/29/2015.
 */

var config=require('./config'),
mongoose=require('mongoose');


module.exports=function(){

    var db=mongoose.connect(config.db);

    require('../app/models/user.server.model');

    return db;
}